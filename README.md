# README #

### CHANGES in last version

* All crawlers are running in headless browser which makes it faster except bet365. It is not working with phantomjs so it is problematical and runs much longer than other spiders. If this is too much problem you can explicitly list crawlers which you want to use as script argument. For example to use all spiders except *bet365_prematch* run script as this:


```
#!python

/usr/bin/run_bet_prematch.sh betpump_prematch superbet_prematch gowager_prematch vbet_prematch
```
 

* added new parameter *accept_timedelta* to *pair_and_export* script.See part of readme *"To pair data from databases and export data"*. Caveat is that when accepting high timedelta time of executing pairing script grows rapidly, because it has to compare many more events.

* Changed logic of pairing events. Created files *teams.csv*, *leagues.csv* and *ignores.csv*

* Changed format of csv file that is result of pair_and_export script.

* * *not_paired* column is used to mark if that event was sucesfully paired with at least one other event if so the value is 'P' otherwise value is 'N'. You can sort file to see all 'N' values and then find out why they were not paired. If so either they have no event to pair or they were not paired because limit of similarity is too high and league is not in *leagues.csv* file.

* * *competition_id* column. This id number is taken from leagues.csv file. You can sort file by this column to see all events from one competition near. Then if some event doesn't have id it is situated on the end of sorted file and you can extend file leagues.csv with these event competition names and append them new id. So next time script is used they will be paired with this id.

## leagues.csv 

This file contains list of competition names. Human expert can use this list to explicitly mark two competition names that are different as one by usin same id number for them. By extending this list precision of pairing script grows rapidly. Format of list is:

```
#!python

competition_name, id
competition_name2, id
...

```

So if two competition names are marked with same id then if events are from competitions with same id there is higher probability they will be paired. If they are from competitions with different id there is no chance to pair them. 
Be careful when creating this list to only use one id for one competition. Mistake will cause mistake in pairing script.
 
## teams.csv

It is similar to leagues.csv but it is used to mark different team names as one if they are highly dissimilar and so pairing script doesn't know they belong to same team. By extending this list precision of pairing script grows.

## ignores.csv 

It is just list of abbreviations used in team names that should be ignored when comparing. For example:
 FC, FK, SV, Atletico, ...

By extending this list precision of pairing script grows.



### What is this repository for? ###

* 
 Project for pulling data from bet sites like bet365, vbet and more using libraries scrapy 1.0.3 and selenium for python version 2.53.6
* Version 0.1

### How do I get set up? ###

Install dependencies with script /usr/bin/run_bet_install.sh

Clone this repository to folder.

```
#!bash

cd /home/ozzy/bet_data
sudo rm -r *
sudo rm -r .*
sudo git clone https://tchovanak@bitbucket.org/tchovanak/bet_data.git .

```

### To Download LIVE MATCH data ###

Use script:

```
#!bash

 /usr/bin/run_bet_live.sh [player1] [player2] [path]
```

player1 is part of player1 name common to all pages. So is player2. 
path is file path where should output be stored.
You can use cron job to schedule start of script.

### To Download PREMATCH data ###


Use script:

```
#!bash

 /usr/bin/run_bet_prematch.sh 
```

### To pair data from databases and export data ###


Use script:

```
#!bash

 /usr/bin/run_bet_pair_and_export.sh [date from in format 'YYYY-MM-DDTHH:MM:SS'] [date to in format 'YYYY-MM-DDTHH:MM:SS'] [limit of similarity] [accept_timedelta] [path] 
```
This script selects data that were collected between dates given as arguments.It pairs records of matches from different databases that are similar in time, player1 name, player2 name and competition name. 

* limit of similarity is float number between 0.0 and 3.0. Higher the number is higher is limit to pair two records as one.

* accept_timedelta is time in hours that should be accepted when two events are higly similar but have other event time different. When difference is less than this parameter two events are paired if they are similar enough.

* path is path where output file should be stored

* script is using csv file teams.csv in same directory. It is used to resolve some very dissimilar player names that are in fact similar.

### To restart scrapyd service running ###

Use script:

```
#!bash

 /usr/bin/run_bet_restart_service.sh 
```


### Caveats ###

* Some pages use a lot of javascript and they have no api to download data.
* Every page uses different league and player names. Sometimes there are differences in match times too. So it is not easy to pair records from different pages. 
* Every page used for pulling bet data is different. Usually they are dynamic pages. E.g bet365.com is flash site which is absolutely not working with scrapy. Therefore we use mobile version of bet365 site mobile.bet365.com