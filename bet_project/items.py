# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class BetProjectItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    time = scrapy.Field()
    cup = scrapy.Field()
    player1 = scrapy.Field()
    player2 = scrapy.Field()
    bet_1 = scrapy.Field()
    bet_2 = scrapy.Field()
    bet_x = scrapy.Field()


class BetLiveItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    time = scrapy.Field()
    act_time = scrapy.Field()
    score = scrapy.Field()
    player1 = scrapy.Field()
    player2 = scrapy.Field()
    bet_1 = scrapy.Field()
    bet_2 = scrapy.Field()
    bet_x = scrapy.Field()


class BetFairLiveItem(scrapy.Item):
    time = scrapy.Field()
    act_time = scrapy.Field()
    score = scrapy.Field()
    player1 = scrapy.Field()
    player2 = scrapy.Field()
    bet_1_1_price = scrapy.Field()
    bet_1_1_size = scrapy.Field()
    bet_1_2_price = scrapy.Field()
    bet_1_2_size = scrapy.Field()
    bet_1_3_price = scrapy.Field()
    bet_1_3_size = scrapy.Field()
    bet_1_4_price = scrapy.Field()
    bet_1_4_size = scrapy.Field()
    bet_1_5_price = scrapy.Field()
    bet_1_5_size = scrapy.Field()
    bet_1_6_price = scrapy.Field()
    bet_1_6_size = scrapy.Field()
    bet_2_1_price = scrapy.Field()
    bet_2_1_size = scrapy.Field()
    bet_2_2_price = scrapy.Field()
    bet_2_2_size = scrapy.Field()
    bet_2_3_price = scrapy.Field()
    bet_2_3_size = scrapy.Field()
    bet_2_4_price = scrapy.Field()
    bet_2_4_size = scrapy.Field()
    bet_2_5_price = scrapy.Field()
    bet_2_5_size = scrapy.Field()
    bet_2_6_price = scrapy.Field()
    bet_2_6_size = scrapy.Field()
    bet_x_1_price = scrapy.Field()
    bet_x_1_size = scrapy.Field()
    bet_x_2_price = scrapy.Field()
    bet_x_2_size = scrapy.Field()
    bet_x_3_price = scrapy.Field()
    bet_x_3_size = scrapy.Field()
    bet_x_4_price = scrapy.Field()
    bet_x_4_size = scrapy.Field()
    bet_x_5_price = scrapy.Field()
    bet_x_5_size = scrapy.Field()
    bet_x_6_price = scrapy.Field()
    bet_x_6_size = scrapy.Field()
    matched = scrapy.Field()
   



