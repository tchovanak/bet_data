# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy import signals
from scrapy.exporters import CsvItemExporter
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from googleapiclient.discovery import build
from googleapiclient.http import BatchHttpRequest
import httplib2
import datetime
from scrapy.exporters import BaseItemExporter
import logging
from difflib import SequenceMatcher
import dateutil.parser
import psycopg2



class PostgreSQLItemExporter(BaseItemExporter):
    """
        Exporter used in pipeline. It exports item to database.
    """

    def __init__(self, spidername, datenow, **kwargs):
        self._configure(kwargs, dont_fail=True)
        self.spidername = spidername
        self.date_now = datenow
        self.connect_to_database(spidername)
        logging.info('Pipeline Initialized')

    def connect_to_database(self, spidername):
        try:
            self.conn = psycopg2.connect(database = spidername, user='postgres', host='localhost', port=5432, password='password')
        except:
            logging.exception("I am unable to connect to the database")
        
    def finish_exporting(self):
        self.conn.commit()
        self.conn.close()
        
    def export_item(self, item):
        """
            Method is responsible for exporting item object given as parameter into postgresql
        """
        #print item
        try:
            cur = self.conn.cursor()
            cur.execute('INSERT INTO items (act_time, time, player1, player2, competition, odd_1, odd_x, odd_2) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)', 
                (self.date_now, item['time'], item['player1'],item['player2'], item['cup'], item['bet_1'], item['bet_x'], item['bet_2']))
            cur.close()
        except:
            logging.exception("INSERT ERROR")


class GoogleSpreadsheetItemExporter(BaseItemExporter):
    """
        Exporter used in pipeline. It exports item to Google spreadsheet.
    """
    # dictionary used to mark starting columns where odds from different pages are stored
    bet_spider_columns = {'superbet': 5, 'gowager': 9, 'betpump': 13, 'vbet': 17, 'bet365': 21}
    
    def __init__(self, wks, spidername, datenow, **kwargs):
        self._configure(kwargs, dont_fail=True)
        self.wks = wks          # worksheet for spreadsheet to export to
        self.prepare_header()
        self.find_last_row()
        self.currentMatchRow = 2
        self.cupRows = []
        self.spidername = spidername
        self.date_now = datenow
        logging.info('Pipeline Initialized')

    def find_last_row(self):
        """
            Finds last row used in spreadsheet
        """
        rangeA = self.wks.range("A1:A"+str(self.wks.row_count))
        self.lastRow = self.wks.row_count
        for cell in rangeA:
            if cell.value == "":
                self.lastRow = cell.row
                break

    def prepare_header(self):
        """
            Prepares spreadsheet header row
        """
        if self.wks.acell("A1").value != "LEAGUE NAME":  # if no header is already there
            self.wks.add_cols(5)
            self.wks.update_acell("A1", "LEAGUE NAME")
            self.wks.update_acell("B1", "MATCH PLAYERS")
            self.wks.update_acell("C1", "MATCH TIME")
            self.wks.update_acell("D1", "DOWNLOAD TIME")
            self.wks.update_acell("E1", "SB ODD 1")
            self.wks.update_acell("F1", "SB ODD X")
            self.wks.update_acell("G1", "SB ODD 2")
            self.wks.update_acell("H1", "SB Percent")
            self.wks.update_acell("I1", "GW ODD 1")
            self.wks.update_acell("J1", "GW ODD X")
            self.wks.update_acell("K1", "GW ODD 2")
            self.wks.update_acell("L1", "GW Percent")
            self.wks.update_acell("M1", "BP ODD 1")
            self.wks.update_acell("N1", "BP ODD X")
            self.wks.update_acell("O1", "BP ODD 2")
            self.wks.update_acell("P1", "BP Percent")
            self.wks.update_acell("Q1", "VB ODD 1")
            self.wks.update_acell("R1", "VB ODD X")
            self.wks.update_acell("S1", "VB ODD 2")
            self.wks.update_acell("T1", "VB Percent")
            self.wks.update_acell("U1", "B365 ODD 1")
            self.wks.update_acell("V1", "B365 ODD X")
            self.wks.update_acell("W1", "B365 ODD 2")
            self.wks.update_acell("X1", "B365 Percent")
            self.wks.update_acell("Y1", "RESULT")
            self.wks.update_acell("Z1", "MAX ODD1")
            self.wks.update_acell("AA1", "MAX ODDX")
            self.wks.update_acell("AB1", "MAX ODD2")

    def export_item(self, item):
        """
            Method is responsible for exporting item object given as parameter into google spreadsheet
        """
        league = str(item['cup'])          # name of league and match as will be displayed in spreadsheet
        match = item['player1'].encode('utf-8') + " vs " + item['player2'].encode('utf-8')
        league_cells = self.wks.findall(league)
        match_row = self.lastRow
        found = False
        # This for cycle looks for same match if it was printed into spreadsheet by another spider then it reuses
        # this row
        sim = 0.0
        for cell in league_cells:
            cellC = self.wks.acell("C"+str(cell.row))
            if str(cellC.value).strip() == str(item['time']):
                cellB = self.wks.acell("B"+str(cell.row))
                sim_loc = similarity(cellB.value, match)
                sim1 = similarity(cellB.value.split(' vs ')[0], match.split(' vs ')[0])
                sim2 = similarity(cellB.value.split(' vs ')[1], match.split(' vs ')[1])
                if (sim1 > 0.4 and sim2 > 0.4) or (sim1 > 0.8) or sim2 > (0.8):
                    found = True    
                    match_row = cell.row
                    break
                
        cell_list = []
        cell_act = 0
        cell = None
        if found:
            cell = self.wks.cell(match_row, self.bet_spider_columns[self.spidername])
            # if there are already bets from this spider previous run on this day then insert new row
        if not found or (cell.value and cell.value != ""):
            match_row = self.lastRow
            self.lastRow += 1
            cell_list = (self.wks.range("A"+str(match_row) + ":C" + str(match_row)))
            cell_list[cell_act].value = league
            cell_list[cell_act+1].value = match.decode('utf-8')
            cell_list[cell_act+2].value = str(item['time'])
            cell_act += 3
        cell_list.append(self.wks.acell("D"+str(match_row)))
        cell_list[cell_act].value = str(self.date_now)
        cell_list.append(self.wks.cell(match_row, self.bet_spider_columns[self.spidername]))
        cell_list.append(self.wks.cell(match_row, self.bet_spider_columns[self.spidername] + 1))
        cell_list.append(self.wks.cell(match_row, self.bet_spider_columns[self.spidername] + 2))
        cell_list[cell_act + 1].value = item['bet_1']
        cell_list[cell_act + 2].value = item['bet_x']
        cell_list[cell_act + 3].value = item['bet_2']
        cell_act += 3
        cell_list.extend(self.wks.range("Z"+str(match_row) + ":AB" + str(match_row)))
        cell_list[cell_act + 1].value = '=MAX(E'+str(match_row)+', I'+str(match_row)+', M'+str(match_row)+', Q'+str(match_row)+', U'+str(match_row)+')'
        cell_list[cell_act + 2].value = '=MAX(F'+str(match_row)+', J'+str(match_row)+', N'+str(match_row)+', R'+str(match_row)+', V'+str(match_row)+')'
        cell_list[cell_act + 3].value = '=MAX(G'+str(match_row)+', K'+str(match_row)+', O'+str(match_row)+', S'+str(match_row)+', W'+str(match_row)+')'
        self.wks.update_cells(cell_list)





class BetProjectPipeline(object):

    @staticmethod
    def create_spreadsheet(service, http, title, users):
        # if there is already such file do not create one
        files = service.files().list(q="title = '" +title+"'").execute(http)
        if len(files['items']) == 0:
            body = {
              'mimeType': 'application/vnd.google-apps.spreadsheet',
              'title': title,
            }
            file = service.files().insert(body=body).execute(http=http)
            batch_request = BatchHttpRequest(callback=batch_callback)
            counter = 1
            for user in users:
                batch_entry = service.permissions().insert(fileId=file['id'], body={
                  'value': user,
                  'type': 'user',
                  'role': 'writer'
                })
                batch_request.add(batch_entry, request_id="batch"+str(counter))
                counter += 1
            batch_request.execute(http)

    @staticmethod
    def delete_files(service, http, title):
         files = service.files().list(q="title = '" +title+"'").execute(http)
         for file in files['items']:
             service.files().delete(fileId=file['id']).execute(http=http)

    @staticmethod
    def find_worksheet(credentials, title):
        gc = gspread.authorize(credentials)
        wks = gc.open(title).sheet1
        return wks

    def __init__(self, download_start_time):
        #self.files = {}
        #scope = ['https://www.googleapis.com/auth/drive', 'https://spreadsheets.google.com/feeds']
        #service = build('drive', 'v2')
        #credentials = ServiceAccountCredentials.from_json_keyfile_name(self.keyfile, scope)
        #http = credentials.authorize(httplib2.Http())
        if download_start_time:
            self.today_date = download_start_time
            if type(download_start_time) is str:
                self.today_date = getDateTimeFromISO8601String(download_start_time)
        else:
            self.today_date = datetime.datetime.now()
        #title = str(self.today_date.year) + "_" + str(self.today_date.month) + "_" + str(self.today_date.day) + "_bet_data"
        # self.delete_files(service, http, title)
        #self.create_spreadsheet(service, http, title, self.users)
        #self.wks = self.find_worksheet(credentials, title)

    @classmethod
    def from_crawler(cls, crawler):
         settings_crawler = crawler.settings
         download_start_time = settings_crawler.get("DOWNLOAD_TIME")
         pipeline = cls(download_start_time)
         crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
         crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
         return pipeline

    def spider_opened(self, spider):
        self.exporter_postgresql = PostgreSQLItemExporter(spider.name,self.today_date)
        self.exporter_postgresql.start_exporting()
        #self.exporter_google_sheet = GoogleSpreadsheetItemExporter(self.wks, spider.name, self.today_date)
        #self.exporter_google_sheet.start_exporting()
        # file = open('%s_odds.csv' % spider.name, 'w+b')
        # self.files[spider] = file
        # self.exporter = CsvItemExporter(file)
        # self.exporter.start_exporting()

    def spider_closed(self, spider):
        #self.exporter_google_sheet.finish_exporting()
        self.exporter_postgresql.finish_exporting()
        spider.driver.quit()
        
        if hasattr(spider, 'display') and spider.display is not None:
            spider.display.stop()
        # self.exporter.finish_exporting()
        # file = self.files.pop(spider)
        # file.close()

    def process_item(self, item, spider):
        self.exporter_postgresql.export_item(item)
        # self.exporter_google_sheet.export_item(item)
        # self.exporter.export_item(item)
        return item


class LiveMatchPipeline(object):

    def __init__(self):
        self.files = {}

    @classmethod
    def from_crawler(cls, crawler):
         pipeline = cls()
         crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
         crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
         return pipeline

    def spider_opened(self, spider):
        file = open('%s%s-%s_%s_odds.csv' % (spider.path,spider.player1, spider.player2, spider.name), 'w+b')
        self.files[spider] = file
        self.exporter = CsvItemExporter(file)
        self.exporter.start_exporting()

    def spider_closed(self, spider):
        spider.driver.quit()
        spider.display.stop()
        self.exporter.finish_exporting()
        file = self.files.pop(spider)
        file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item


def batch_callback(request_id, response, exception):
  print "Response for request_id (%s):" % request_id
  print response

  # Potentially log or re-raise exceptions
  if exception:
    raise exception


def similarity(a,b):
    return SequenceMatcher(None, a, b).ratio()

def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)
    if len(s2) == 0:
        return len(s1)
    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row

    return previous_row[-1]

def getDateTimeFromISO8601String(s):
    d = dateutil.parser.parse(s)
    return d