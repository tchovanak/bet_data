# -*- coding: utf-8 -*-
import scrapy
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime as dt
from selenium.common.exceptions import TimeoutException
from bet_project.items import BetProjectItem
import datetime
import logging
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from pyvirtualdisplay import Display
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time


class Bet365PrematchSpider(scrapy.Spider):
    """
        Crawler for mobile.bet365.com
    """
    name = "bet365_prematch"
    allowed_domains = ["mobile.bet365.com"]
    wait_long = 30  # how many seconds should webdriver wait for response when it is not avoidable  
    wait_short = 15 # how many seconds should webdriver wait for response that is avoidable
    # set language now - locale is slovak 26, english is 1
    start_urls = ['https://mobile.bet365.com/#type=Splash;key=1;ip=0;lng=1']
    custom_settings = {
        'ITEM_PIPELINES': {
            'bet_project.pipelines.BetProjectPipeline': 300,
        }
    }

    month_dict = {"jan": 1, "feb": 2, "mar": 3, "apr": 4, "máj": 5, "may": 5, "jun": 6, "jún": 6, "júl": 7, "jul": 7,
                  'aug': 8, 'sep': 9, 'okt': 10, 'oct': 10, 'nov': 11, 'dec': 12}


    def __init__(self, **kwargs):
        #self.driver = webdriver.PhantomJS()
        self.display = Display(visible=0, size=(1400, 1024))
        self.display.start()
        self.driver = get_webdriver(locale='en-us')
        self.driver.set_window_size(1400, 1024)
        self.wait = WebDriverWait(self.driver, self.wait_long)

    def parse(self, response):
        self.driver.get(response.url)
        attempts = 0
        result = False
        while attempts <= 5:   # this section throws stale element exception sometimes therefore
                               # it is tried multiple times
            try:
                link = self.wait.until(EC.presence_of_element_located((By.XPATH,
                                   '//span[contains(@class,"sb-SportsItem_Truncator") and (contains(text(),"Soccer") or contains(text(),"Futbal"))]')))
                self.driver.execute_script("arguments[0].click();", link)
                div = self.wait.until(EC.presence_of_element_located((By.XPATH,
                                       '//div[contains(@id,"SplashContent")]//h1/em[contains(text(),"Konečný výsledok") or '
                                       'contains(text(),"Full Time Result")]/ancestor::div[contains(@class,"enhancedPod")]')))
                cl = div.get_attribute('class')
                if "collapsed" in cl:
                    self.driver.execute_script("arguments[0].click();", link)
                links = self.wait.until(EC.presence_of_all_elements_located((By.XPATH,
                                       '//div[contains(@id,"SplashContent")]//h1/em[contains(text(),"Konečný výsledok") or '
                                       'contains(text(),"Full Time Result")]/ancestor::div[contains(@class,"enhancedPod")]//h3')))    
                for link in links:
                    cl = link.get_attribute("class")
                    if "collapsed" in cl:
                        self.driver.execute_script("arguments[0].click();", link)
                result = True

                break
            except StaleElementReferenceException :
                if attempts == 5:
                    logging.exception("Too many attempts with stale element reference exception")
                attempts += 1
            except TimeoutException:
                logging.exception('BET365: Timeout exception when trying to access xpath.')
                break
        if not result:
            logging.exception("BET365: After 5 attempts no h3 element")
        time.sleep(5)
        try:
            links = WebDriverWait(self.driver, self.wait_long).until(
                    EC.presence_of_all_elements_located((By.XPATH,
                        '//div[contains(@id,"SplashContent")]//h1/em[contains(text(),"Konečný výsledok") or contains(text(),"Full Time Result")]/ancestor::div[contains(@class,"enhancedPod")]//div[contains(@class,"podSplashRow") and not(contains(./preceding::h3[1]//text(),"Hlavné zoznamy")) and not(contains(./preceding::h3[1]//text(),"Main List"))]//span')))
        except TimeoutException:
            self.driver.save_screenshot('screenshot.png')
            logging.exception('BET365: Timeout exception when trying to access xpath')
            return     
        size = len(links)
        for i in range(0, size - 1):
            attempts = 0
            result = False
            while attempts <= 5:   # this section throws stale element exception sometimes therefore
                               # it is tried multiple times
                try:
                    go_back = False
                    links = WebDriverWait(self.driver, self.wait_short).until(
                        EC.presence_of_all_elements_located((By.XPATH,
                            '//div[contains(@id,"SplashContent")]//h1/em[contains(text(),"Konečný výsledok") or contains(text(),"Full Time Result")]/ancestor::div[contains(@class,"enhancedPod")]//div[contains(@class,"podSplashRow") and not(contains(./preceding::h3[1]//text(),"Hlavné zoznamy")) and not(contains(./preceding::h3[1]//text(),"Main List"))]//span')))
                    link = links[i]
                    cup = link.text.encode('utf-8', 'replace').strip()
                    self.driver.execute_script("arguments[0].click();", link)
                    go_back = True
                    link = self.wait.until(EC.presence_of_element_located((By.XPATH,
                                '//div[contains(@id,"Coupon")]')))
                    selector = scrapy.Selector(text=link.get_attribute('innerHTML'))
                    rows = selector.xpath('//div[contains(@class,"podHeaderRow") or contains(@class,"podEventRow")]')
                    result = True
                    break
                except StaleElementReferenceException :
                    if attempts == 5:
                        logging.exception("Too many attempts with stale element reference exception")
                        break
                    attempts += 1
                except TimeoutException:
                    logging.warning('BET365: Timeout exception when trying to access xpath')
                    if go_back:
                        self.driver.execute_script('window.history.go(-1);')
                    break  
            if not result:
                continue
            if not rows or len(rows) == 0:
                logging.error('BET365: no result trying to access league')
                self.driver.execute_script('window.history.go(-1);')
                continue
            actual_date = None
            for row in rows:
                try:
                    cl = row.xpath('./@class').extract()
                    if "podHeaderRow" in cl:
                        header = row.xpath('./div[contains(@class,"wideLeftColumn")]/text()').extract()
                        if len(header) > 0:
                            splitted = header[0].strip().split(" ")
                            today = dt.now()
                            actual_date = datetime.datetime(today.year,
                                                        self.month_dict[splitted[2].encode('utf-8').lower()],
                                                        int(splitted[1].encode('utf-8')))
                        else:
                            actual_date = dt.now()
                    else:
                        players = row.xpath('.//span[contains(@class,"ippg-Market_Truncator")]//text()').extract()
                        if not players or len(players) <= 0:
                            continue
                        game_times = row.xpath('.//div[contains(@class,"ippg-Market_GameStartTime")]//text()').extract()
                        if not game_times or len(game_times) <= 0:
                            continue
                        game_time = game_times[0].strip().split(':')
                        actual_time = datetime.datetime(actual_date.year, actual_date.month, actual_date.day,
                                                        int(game_time[0]), int(game_time[1]))
                        odds = row.xpath('.//span[contains(@class,"ippg-Market_Odds")]//text()').extract()
                        if not odds or len(odds) < 3:
                            odds = ["0.0","0.0","0.0"]
                        bet_project_item = BetProjectItem()
                        bet_project_item['cup'] = cup
                        bet_project_item['time'] = actual_time.isoformat()
                        bet_project_item['player1'] = players[0].strip()
                        bet_project_item['player2'] = players[1].strip()
                        bet_project_item['bet_1'] = odds[0].strip()
                        bet_project_item['bet_x'] = odds[1].strip()
                        bet_project_item['bet_2'] = odds[2].strip()
                        logging.warning(bet_project_item)
                        yield bet_project_item
                except:
                    logging.exception()
                    continue
            self.driver.execute_script('window.history.go(-1);')
        self.driver.close()


def get_webdriver(attempts=3, timeout=60, locale='en-us'):
  firefox_profile = webdriver.FirefoxProfile()
  #firefox_profile.set_preference("permissions.default.stylesheet", 2);
  #firefox_profile.set_preference("permissions.default.image", 2);
  firefox_profile.set_preference("intl.accept_languages", locale)
  firefox_profile.update_preferences()

  driver = webdriver.Firefox(firefox_profile=firefox_profile)

  return driver