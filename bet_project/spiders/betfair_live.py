import scrapy
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import logging
import threading
import time
from bet_project.items import BetFairLiveItem
from retrying import retry
from pyvirtualdisplay import Display
import datetime

def setInterval(interval):
	def decorator(function):
		def wrapper(*args, **kwargs):
			stopped = threading.Event()

			def loop(): # executed in another thread
				while not stopped.wait(interval): # until stopped
					kwargs['stop'] = stopped
					function(*args, **kwargs)

			t = threading.Thread(target=loop)
			t.daemon = True # stop if the program exits
			t.start()
			return stopped
		return wrapper
	return decorator

class BetFairLiveSpider(scrapy.Spider):

	name = "betfair_live"
	allowed_domains = ["identitysso.betfair.com", 'betfair.com']
	start_urls = ['https://identitysso.betfair.com/view/login']
	player1 = ""
	player2 = ""
	custom_settings = {
	    'ITEM_PIPELINES': {
	        'bet_project.pipelines.LiveMatchPipeline': 400,
	    }
	}
	items = []

	def __init__(self, player1, player2,path, **kwargs):
		# self.driver = webdriver.PhantomJS("g:/workspace_bet_scraper/phantomjs-2.1.1-windows/bin/phantomjs.exe")
		self.display = Display(visible=0, size=(1400, 1024))
		self.display.start()
		self.player1 = player1
		self.player2 = player2
		self.path = path
		self.driver = webdriver.Firefox()
		self.driver.set_window_size(1400, 1024)
		self.wait = WebDriverWait(self.driver, 10)
		self.threads = []

	def parse(self, response):
		self.driver.get(response.url)
		try:
		    input_name = self.wait.until(EC.presence_of_element_located((By.XPATH,
		                    '//input[@id="username"]')))
		    input_name.send_keys("tchovanak@gmail.com")
		    input_password = self.wait.until(EC.presence_of_element_located((By.XPATH,
		                    '//input[@id="password"]')))
		    input_password.send_keys("cba456123")
		    submit = self.wait.until(EC.presence_of_element_located((By.XPATH,
		                    '//button[@id="login"]')))
		    self.driver.execute_script("arguments[0].click();", submit)
		    time.sleep(10)
		except TimeoutException:
		    logging.exception('BETFair_LIVE: Timeout exception when trying to login')
		    return

		self.driver.get("https://www.betfair.com/exchange/inplay")

		try:
			WebDriverWait(self.driver,1000,10).until(EC.presence_of_element_located((By.XPATH,'//span[contains(@class,"home-team") and contains(text(), "'+self.player1+'")]')))
			link = WebDriverWait(self.driver,1000,10).until(EC.presence_of_element_located((By.XPATH,'//span[contains(@class,"away-team") and contains(text(), "'+self.player2+'")]')))
			self.driver.execute_script("arguments[0].click();", link)
			link = WebDriverWait(self.driver,10,1).until(EC.presence_of_element_located((By.XPATH,'//a[contains(text(),"View full market")]')))
			self.driver.execute_script("arguments[0].click();", link)
			
			#self.dateelement = WebDriverWait(self.driver,20).until(EC.presence_of_element_located((By.XPATH,'//span[contains(@class,"time")]')))
		except TimeoutException as e:
			logging.exception("BETFAIR_LIVE : Timeout")
			return
		
		stop = self.get_odds()

		self.time_to_end = datetime.datetime.now() + datetime.timedelta(minutes=130)

		while not stop.wait(60):
			if datetime.datetime.now() > self.time_to_end:
				stop.set()
		
		for item in self.items:
			yield item

	@setInterval(1.0)
	def get_odds(self,*args, **kwargs):
		try:
			self.tbody = WebDriverWait(self.driver,20).until(EC.presence_of_element_located((By.XPATH,'//h3[contains(text(),"'+self.player1+'")]/ancestor::tbody')))
			selector_bets = scrapy.Selector(text=self.tbody.get_attribute('innerHTML'))
			#time = selector_time.xpath('.//text()').extract()
			#score = selector.xpath('.//td[contains(@class,"score")]//text()').extract()
			bets_prices = selector_bets.xpath('.//span[contains(@class, "bet-button-price")]//text()').extract()
			bets_sizes = selector_bets.xpath('.//span[contains(@class, "bet-button-size")]//text()').extract()
		except:
			logging.exception("BETFAIR_LIVE: Exception")
			return
		betliveitem = BetFairLiveItem()
		try:
			element = self.driver.find_element_by_xpath('//p[contains(@class,"time-elapsed")]//span')
			timeelement = element.text
			betliveitem['time'] = timeelement
			elements = self.driver.find_elements_by_xpath('//span[contains(@class,"score")]//span')
			logging.warning(timeelement)
			if elements and len(elements) > 2:
 				scoreelement = str(elements[0].get_attribute("innerHTML")) + str(elements[1].get_attribute("innerHTML")) + str(elements[2].get_attribute("innerHTML"))
				betliveitem['score'] = scoreelement
		except:
			logging.warning("BETFAIR_LIVE: Exception")
			self.driver.refresh()
		try:
			betliveitem['player1'] = self.player1
			betliveitem['player2'] = self.player2
			betliveitem['act_time'] = datetime.datetime.now()
			len_prices = len(bets_prices) 
			len_sizes = len(bets_sizes) 
			if len_prices > 0: betliveitem['bet_1_1_price'] = bets_prices[0]
			if len_prices > 1: betliveitem['bet_1_2_price'] = bets_prices[1]
			if len_prices > 2: betliveitem['bet_1_3_price'] = bets_prices[2]
			if len_prices > 3: betliveitem['bet_1_4_price'] = bets_prices[3]
			if len_prices > 4: betliveitem['bet_1_5_price'] = bets_prices[4]
			if len_prices > 5: betliveitem['bet_1_6_price'] = bets_prices[5]
			if len_prices > 6: betliveitem['bet_2_1_price'] = bets_prices[6]
			if len_prices > 7: betliveitem['bet_2_2_price'] = bets_prices[7]
			if len_prices > 8: betliveitem['bet_2_3_price'] = bets_prices[8]
			if len_prices > 9: betliveitem['bet_2_4_price'] = bets_prices[9]
			if len_prices > 10: betliveitem['bet_2_5_price'] = bets_prices[10]
			if len_prices > 11: betliveitem['bet_2_6_price'] = bets_prices[11]
			if len_prices > 12: betliveitem['bet_x_1_price'] = bets_prices[12]
			if len_prices > 13: betliveitem['bet_x_2_price'] = bets_prices[13]
			if len_prices > 14: betliveitem['bet_x_3_price'] = bets_prices[14]
			if len_prices > 15: betliveitem['bet_x_4_price'] = bets_prices[15]
			if len_prices > 16: betliveitem['bet_x_5_price'] = bets_prices[16]
			if len_prices > 17: betliveitem['bet_x_6_price'] = bets_prices[17]

			if len_sizes > 0: betliveitem['bet_1_1_size'] = bets_sizes[0]
			if len_sizes > 1: betliveitem['bet_1_2_size'] = bets_sizes[1]
			if len_sizes > 2: betliveitem['bet_1_3_size'] = bets_sizes[2]
			if len_sizes > 3: betliveitem['bet_1_4_size'] = bets_sizes[3]
			if len_sizes > 4: betliveitem['bet_1_5_size'] = bets_sizes[4]
			if len_sizes > 5: betliveitem['bet_1_6_size'] = bets_sizes[5]
			if len_sizes > 6: betliveitem['bet_2_1_size'] = bets_sizes[6]
			if len_sizes > 7: betliveitem['bet_2_2_size'] = bets_sizes[7]
			if len_sizes > 8: betliveitem['bet_2_3_size'] = bets_sizes[8]
			if len_sizes > 9: betliveitem['bet_2_4_size'] = bets_sizes[9]
			if len_sizes > 10: betliveitem['bet_2_5_size'] = bets_sizes[10]
			if len_sizes > 11: betliveitem['bet_2_6_size'] = bets_sizes[11]
			if len_sizes > 12: betliveitem['bet_x_1_size'] = bets_sizes[12]
			if len_sizes > 13: betliveitem['bet_x_2_size'] = bets_sizes[13]
			if len_sizes > 14: betliveitem['bet_x_3_size'] = bets_sizes[14]
			if len_sizes > 15: betliveitem['bet_x_4_size'] = bets_sizes[15]
			if len_sizes > 16: betliveitem['bet_x_5_size'] = bets_sizes[16]
			if len_sizes > 17: betliveitem['bet_x_6_size'] = bets_sizes[17]
			logging.warning(betliveitem)
			self.items.append(betliveitem)
		except:
			logging.exception("BETFAIR_LIVE: Exception")
			return
		
	