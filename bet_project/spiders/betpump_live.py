import scrapy
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import logging
import threading
import time
from bet_project.items import BetLiveItem
from retrying import retry
from pyvirtualdisplay import Display
import datetime


def setInterval(interval):
	def decorator(function):
		def wrapper(*args, **kwargs):
			stopped = threading.Event()

			def loop(): # executed in another thread
				while not stopped.wait(interval): # until stopped
					kwargs['stop'] = stopped
					function(*args, **kwargs)

			t = threading.Thread(target=loop)
			t.daemon = True # stop if the program exits
			t.start()
			return stopped
		return wrapper
	return decorator

class BetPumpLiveSpider(scrapy.Spider):

	name = "betpump_live"
	allowed_domains = ["game.betpump.com"]
	start_urls = ['http://game.betpump.com/#/']
	player1 = ""
	player2 = ""
	custom_settings = {
	    'ITEM_PIPELINES': {
	        'bet_project.pipelines.LiveMatchPipeline': 400,
	    }
	}
	items = []

	def __init__(self, player1, player2, path, **kwargs):
		# self.driver = webdriver.PhantomJS("g:/workspace_bet_scraper/phantomjs-2.1.1-windows/bin/phantomjs.exe")
		self.display = Display(visible=0, size=(1400, 1024))
		self.display.start()
		self.player1 = player1
		self.player2 = player2
		self.path = path
		self.driver = webdriver.Firefox()
		self.driver.set_window_size(1400, 1024)
		self.wait = WebDriverWait(self.driver, 10)
		self.threads = []

	def parse(self, response):
		self.driver.get(response.url)
		try:
		    self.wait.until(EC.presence_of_element_located((By.XPATH,
		                    '//a[text()="Soccer"]')))
		except TimeoutException:
		    logging.exception('BETPUMP_LIVE: Timeout exception when trying to access xpath : '
		                          '//a[text()="Soccer"]')
		    return
		try:
			link_to_match = self.wait.until(EC.presence_of_element_located((By.XPATH,
						 '//span[contains(@class,"event-description") and contains(text(),"'+self.player1+'") and contains(text(),"'+self.player2+'")]')))
			self.driver.execute_script("arguments[0].click();", link_to_match)
		except TimeoutException:
		    logging.exception('BETPUMP_LIVE:')


		try:
			WebDriverWait(self.driver,1000,10).until(EC.presence_of_element_located((By.XPATH,'//span[contains(@class,"game-info-time")]')))
			self.outer_div_element = WebDriverWait(self.driver,20).until(
				EC.presence_of_element_located(
				(By.XPATH, '//div[contains(@class,"container-fluid")]')))
		except TimeoutException as e:
			logging.exception("BETPUMP_LIVE : Timeout")
			return
			
		stop = self.get_odds()

		self.time_to_end = datetime.datetime.now() + datetime.timedelta(minutes=130)

		while not stop.wait(60):
			if datetime.datetime.now() > self.time_to_end:
				stop.set()
		for item in self.items:
			yield item

	@setInterval(10.0)
	def get_odds(self,*args, **kwargs):
		try:
			selector = scrapy.Selector(text=self.outer_div_element.get_attribute('innerHTML'))
			period = selector.xpath('.//span[contains(@class,"game-info-period")]//text()').extract()
			score = selector.xpath('.//td[contains(@class,"score")]//text()').extract()
			time = selector.xpath('.//span[contains(@class,"game-info-time")]//text()').extract()
			bets_panel = selector.xpath('.//h4[contains(text(),"Match Result")]/parent::div')
			bets = bets_panel.xpath('.//button[contains(@class,"btn-betting")]//span[contains(@class,"btn-info")]//text()').extract()
			logging.warning(time)
		except:
			return
		if not bets or len(bets) < 3: 
			bets = ["0.0","0.0","0.0"]
		if period and period[0].strip() == 'FT':
			stop = kwargs['stop']
			stop.set()
			return
		if not time or len(time) == 0:
			return
		try:
			bet1 = bets[0].strip()
			betx = bets[1].strip()
			bet2 = bets[2].strip()
			#if bet1 != self.last_bets[0] or betx != self.last_bets[1] or bet2 != self.last_bets[2]:
				#self.last_bets[0] = bet1
				#self.last_bets[1] = betx
				#self.last_bets[2] = bet2
			betliveitem = BetLiveItem()
			betliveitem['player1'] = self.player1
			betliveitem['player2'] = self.player2
			betliveitem['bet_1'] = bet1
			betliveitem['bet_x'] = betx
			betliveitem['bet_2'] = bet2
			if score and len(score) >= 2:
				betliveitem['score'] = score[0].strip() + ':' + score[1].strip()
			if time and len(time) >= 1:
				betliveitem['time'] = time[0].strip()
			betliveitem['act_time'] = datetime.datetime.now()
			#logging.warning(betliveitem)
			self.items.append(betliveitem)
		except:
			logging.exception("BETPUMP LIVE: Exception ")
			return