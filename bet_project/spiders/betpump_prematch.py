import scrapy
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bet_project.items import BetProjectItem
from datetime import datetime as dt
import datetime
import logging
from selenium.common.exceptions import TimeoutException
from pyvirtualdisplay import Display


class BetPumpPrematchSpider(scrapy.Spider):
    """
        Crawler for mobile.bet365.com
    """
    name = "betpump_prematch"
    allowed_domains = ["game.betpump.com"]
    wait_long  = 30  # how many seconds should webdriver wait for response when it is not avoidable  
    wait_short = 15 # how many seconds should webdriver wait for response that is avoidable
    start_urls = ['http://game.betpump.com/#/']
    custom_settings = {
        'ITEM_PIPELINES': {
            'bet_project.pipelines.BetProjectPipeline': 300,
        }
    }

    def __init__(self, **kwargs):
        self.driver = webdriver.PhantomJS()
        #self.display = Display(visible=0, size=(1400, 1024))
        #self.display.start()
        #self.driver = webdriver.Firefox()
        self.driver.set_window_size(1400, 1024)
        self.wait = WebDriverWait(self.driver, self.wait_long)

    def parse(self, response):
        self.driver.get(response.url)
        try:
            link = self.wait.until(EC.presence_of_element_located((By.XPATH,
                            '//a[contains(text(),"Soccer")]')))
            links = WebDriverWait(self.driver, self.wait_short).until(EC.presence_of_all_elements_located((By.XPATH,
                                         '//div[contains(@class,"panel-default") and contains(.//h4//text(), "Soccer")]//div[contains(@class,"sub-category-inner")]//span[contains(@class,"event-description coupon")]'))) #
        except:
            logging.exception('BETPUMP: Timeout exception when trying to access xpath : ')
            return

        i = 0
        for i in range(0,len(links)):
            try:
                links = WebDriverWait(self.driver, self.wait_short).until(EC.presence_of_all_elements_located((By.XPATH,
                                         '//div[contains(@class,"panel-default") and contains(.//h4//text(), "Soccer")]//div[contains(@class,"sub-category-inner")]//span[contains(@class,"event-description coupon")]'))) #
            except:
                logging.exception('BETPUMP: Timeout exception when trying to access xpath : ')
                return
            if i >= len(links):
                break
            link = links[i]
            self.driver.execute_script("arguments[0].click();", link)
            league = str(link.get_attribute("innerHTML"))[:-14]
            try:
                div = self.wait.until(EC.presence_of_element_located((By.XPATH,
                                '//div[contains(@class,"markets-panel")]//h4/span[1]')))
                div = self.wait.until(EC.presence_of_element_located((By.XPATH,
                                '//div[contains(@class,"markets-panel")]')))
               
            except TimeoutException:
                logging.exception('BETPUMP: Timeout exception when trying to access xpath : ')
                continue
            try:
                
                markets_panel = scrapy.Selector(text=div.get_attribute('innerHTML'))
                times = markets_panel.xpath('.//h4/span[1]/text()').extract()
                players = markets_panel.xpath('.//h4/span[2]/text()').extract()
                odds = markets_panel.xpath('.//button/span[contains(@class,"btn-info")]/text()').extract()
            except:
                logging.exception("Exception")
                continue
            player_counter = 0
            odd_counter = 0
            print times
            for t in times:
                try:
                    bet_project_item = BetProjectItem()
                    bet_project_item['cup'] = league
                    timeparsed = dt.strptime(t.strip(), '%d %b %H:%M')
                    timetoday = datetime.datetime.now()
                    timetosave = datetime.datetime(timetoday.year, timeparsed.month, timeparsed.day,
                                                   timeparsed.hour, timeparsed.minute)
                    bet_project_item['time'] = timetosave.isoformat()
                    players12 = players[player_counter].strip().split(' v ')
                    bet_project_item['player1'] = players12[0]
                    bet_project_item['player2'] = players12[1]
                    player_counter += 1
                    if not odds or (len(odds) <= odd_counter) or odds[odd_counter] == 'N/A' or odds[odd_counter] == '':
                        bet_project_item['bet_1'] = '0.0'
                    else:
                        bet_project_item['bet_1'] = odds[odd_counter]
                    odd_counter += 1
                    if not odds or (len(odds) <= odd_counter) or odds[odd_counter] == 'N/A' or odds[odd_counter] == '':
                        bet_project_item['bet_x'] = '0.0'
                    else:
                        bet_project_item['bet_x'] = odds[odd_counter]
                    odd_counter += 1
                    if not odds or (len(odds) <= odd_counter) or odds[odd_counter] == 'N/A' or odds[odd_counter] == '':
                        bet_project_item['bet_2'] = '0.0'
                    else:
                        bet_project_item['bet_2'] = odds[odd_counter]
                    odd_counter += 1
                    logging.warning(bet_project_item)
                    yield bet_project_item
                except:
                    logging.exception('exception occured')
                    continue
        self.driver.close()
