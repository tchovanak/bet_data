import scrapy
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import logging
import threading
import time
from bet_project.items import BetLiveItem
from pyvirtualdisplay import Display
import retrying
import datetime


def setInterval(interval):
	def decorator(function):
		def wrapper(*args, **kwargs):
		    stopped = threading.Event()

		    def loop(): # executed in another thread
				while not stopped.wait(interval): # until stopped
					kwargs['stop'] = stopped
					function(*args, **kwargs)

		    t = threading.Thread(target=loop)
		    t.daemon = True # stop if the program exits
		    t.start()
		    return stopped
		return wrapper
	return decorator

def retry_if_result_none(result):
    """Return True if we should retry (in this case when result is None), False otherwise"""
    return result is None

class GoWagerLiveSpider(scrapy.Spider):

	last_bets = [2.0,1.0,0.0]
	name = "gowager_live"
	allowed_domains = ["gowager.co.uk"]
	start_urls = ['https://www.gowager.co.uk/sportsbook/SOCCER/']
	player1 = ""
	player2 = ""
	path = ""	
	custom_settings = {
	    'ITEM_PIPELINES': {
	        'bet_project.pipelines.LiveMatchPipeline': 400,
	    }
	}
	items = []

	def __init__(self, player1, player2,path, **kwargs):
	    # self.driver = webdriver.PhantomJS("g:/workspace_bet_scraper/phantomjs-2.1.1-windows/bin/phantomjs.exe")
		self.display = Display(visible=0, size=(1400, 1024))
		self.display.start()
		self.player1 = player1
		self.player2 = player2
		self.path = path
		self.driver = webdriver.Firefox()
		self.driver.set_window_size(1400, 1024)
		self.wait = WebDriverWait(self.driver, 10)


	def parse(self, response):
		self.driver.get(response.url)
		try:
		    link = self.wait.until(EC.presence_of_element_located((By.XPATH,
		                           '//span[contains(@id,"priceToggle") and contains(./strong/text(),"fractional")]'))) # toggles displayed price from fractional to decimal
		    self.driver.execute_script("arguments[0].click();", link)
		    self.wait.until(EC.presence_of_element_located((By.XPATH,
		                    '//span[contains(@id,"priceToggle") and contains(./strong/text(),"decimal")]')))
		except TimeoutException:
		    logging.warning('GOWAGER: Timeout exception when trying to toggle fractional to decimal on xpath:'
		                      ' //span[contains(@id,"priceToggle")]')
		
		
		try:
			link = WebDriverWait(self.driver, 1000, 10).until(self.wait_for_match(self.driver))
		except TimeoutException:
			logging.exception('GOWAGER_LIVE: Timeout exception')
			return
		
		self.time_to_end = datetime.datetime.now() + datetime.timedelta(minutes=130)

		stop = self.get_odds()
		
		while not stop.wait(60):
			if datetime.datetime.now() > self.time_to_end:
				stop.set()

		for item in self.items:
			yield item



	def wait_for_match(self, driver):
		print "refresh"
		self.driver.refresh()
		return EC.presence_of_element_located((By.XPATH,'//a[contains(text()[1],"'+self.player1+'") and contains(text()[2],"'+self.player2+'")]/ancestor::li[contains(@class,"single-event live")]'))


	@setInterval(10.0)
	def get_odds(self,*args, **kwargs):
		logging.warning("get_odds")
		try: 
			outer_li_element = WebDriverWait(self.driver,5).until(
				EC.presence_of_element_located(
				(By.XPATH, '//a[contains(./text()[1],"'+self.player1+'") and contains(./text()[2],"'+self.player2+'")]/ancestor::li[contains(@class,"single-event live")]')))
		except TimeoutException:
			logging.warning('GOWAGER_LIVE: Match ended ' + self.player1 + ' v ' + self.player2)
			stop = kwargs['stop']
			stop.set()
			return
		try:	
			selector = scrapy.Selector(text=outer_li_element.get_attribute('innerHTML'))
			time = selector.xpath('.//span[contains(@class,"time")]//text()').extract()
			score = selector.xpath('.//span[contains(@class,"score")]//text()').extract()
			bets = selector.xpath('.//div[contains(@class,"single-event__bets__price")]//text()').extract()
			logging.warning(time)
		except:
			logging.warning('GOWAGER_LIVE: Match ended ' + self.player1 + ' v ' + self.player2)
			#stop = kwargs['stop']
			#stop.set()
			return
		if not bets or len(bets) < 3:
			bets = ["0.0","0.0","0.0"]
		if not time or len(time) == 0:
			logging.warning('GOWAGER_LIVE: Match ended ' + self.player1 + ' v ' + self.player2)
			#stop = kwargs['stop']
			#top.set()
			return
		if time[0].strip() == 'FIN':
			logging.warning('GOWAGER_LIVE: Match ended ' + self.player1 + ' v ' + self.player2)
			stop = kwargs['stop']
			stop.set()
			return
		try:
			bet1 = bets[0].strip()
			betx = bets[1].strip()
			bet2 = bets[2].strip()
			#if bet1 != self.last_bets[0] or betx != self.last_bets[1] or bet2 != self.last_bets[2]:
				#self.last_bets[0] = bet1
				#self.last_bets[1] = betx
				#self.last_bets[2] = bet2
			betliveitem = BetLiveItem()
			betliveitem['player1'] = self.player1
			betliveitem['player2'] = self.player2
			betliveitem['bet_1'] = bet1
			betliveitem['bet_x'] = betx
			betliveitem['bet_2'] = bet2
			if score and len(score) > 0:
				betliveitem['score'] = score[0].strip()
			if time and len(time) > 0:
				betliveitem['time'] = time[0].strip()
			betliveitem['act_time'] = datetime.datetime.now()
			self.items.append(betliveitem)
			logging.info(betliveitem)
		except:
			logging.exception("GOWAGER_LIVE : Unable to save item")
		#
		
			

