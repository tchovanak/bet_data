# -*- coding: utf-8 -*-
import scrapy
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime as dt
import re
import datetime
from bet_project.items import BetProjectItem
from selenium.common.exceptions import TimeoutException
import logging
from exceptions import ValueError
from pyvirtualdisplay import Display
from selenium import webdriver


class GoWagerSpider(scrapy.Spider):
    """
        Crawler for gowager.co.uk
    """
    name = "gowager_prematch"
    allowed_domains = ["gowager.co.uk"]
    wait_short = 3  # how many seconds should webdriver wait for response that is avoidable
    wait_long = 10 # how many seconds should webdriver wait for response when it is not avoidable  
    start_urls = ['https://www.gowager.co.uk/sportsbook/SOCCER/']
    custom_settings = {
        'ITEM_PIPELINES': {
            'bet_project.pipelines.BetProjectPipeline': 300,
        }
    }
        

    def __init__(self, **kwargs):
        self.driver = webdriver.PhantomJS()
        #self.display = Display(visible=0, size=(1400, 1024))
        #self.display.start()
        #self.driver = webdriver.Firefox()
        self.driver.set_window_size(1400, 1024)
        self.wait = WebDriverWait(self.driver, self.wait_long)

    def parse(self, response):
        self.driver.get(response.url)
        try:
            link = self.wait.until(EC.presence_of_element_located((By.XPATH,
                                   '//span[contains(@id,"priceToggle") and contains(./strong/text(),"fractional")]'))) # toggles displayed price from fractional to decimal
            self.driver.execute_script("arguments[0].click();", link)
            self.wait.until(EC.presence_of_element_located((By.XPATH,
                            '//span[contains(@id,"priceToggle") and contains(./strong/text(),"decimal")]')))
        except TimeoutException:
            logging.warning('GOWAGER: Timeout exception when trying to toggle fractional to decimal on xpath:'
                              ' //span[contains(@id,"priceToggle")]')
            
        try:
            links = self.wait.until(EC.presence_of_all_elements_located((By.XPATH,
                        '(//h3[contains(text(),"All Competitions")]/following-sibling::div)[1]//li//a'))) 
                        #'(//h3[contains(text(),"All Competitions")]/following-sibling::div)[1]//li//a)))'))) 
        except TimeoutException:
            logging.exception('GOWAGER: Timeout exception ')
            return       
        urls = []
        for link in links: 
            league_url = str(link.get_attribute('href'))
            league = link.text
            urls.append([league, league_url])
        for url in urls:   # parse each cup
            request = scrapy.Request(url[1], callback=self.parse_cup)
            request.meta["league"] = url[0]
            yield request

    def parse_cup(self, response):
        cup = response.meta["league"]
        #selector = response.selector
        self.driver.get(response.url)
        try:
            WebDriverWait(self.driver, self.wait_short).until(
                EC.presence_of_element_located((By.XPATH, '//a[contains(text(),"Match Coupon")]')))
            WebDriverWait(self.driver, self.wait_short).until(
                EC.presence_of_element_located((By.XPATH, '//div[contains(@class,"single-event__players")]')))
        except TimeoutException:
            logging.warning('GOWAGER: Timeout exception when trying to access xpath for cup ' + cup)
            return

        selector = scrapy.Selector(text=self.driver.page_source)
        days = None
        try:
            days = selector.xpath('//div[contains(@class,"single-event__players")]')
            if not days or len(days) == 0:
                logging.warning('GOWAGER: No node was found with xpath: //div[contains(@class,"single-event__players")]/text()')
                return
        except:
            logging.exception('GOWAGER: No node was found with xpath: //div[contains(@class,"single-event__players")]/text()')
            return
        for d in days:
            text = d.xpath("./text()").extract()[0].strip()
            if not text or text == "":
                continue
            date_in = re.sub(r'(\d{1,2})(st|nd|rd|th)', r'\1', text)
            struct_time = dt.strptime(date_in, "%A %d %B")
            try:
                link = d.xpath('./ancestor::div/following-sibling::div[contains(@class,"app__content")]')
                times = link.xpath('.//span[@class="time" and not(./preceding-sibling/@class="score")]//text()').extract()
                players = link.xpath('.//div[contains(@class,"single-event__players")]/a//text()').extract()
                bets = link.xpath('.//div[contains(@class, "single-event__bets__price")]/text()').extract()
            except:
                logging.exception("Exception")
                return

            players_cursor = 0
            bets_cursor = 0
            for time in times:
                try:
                    bet_project_item = BetProjectItem()
                    bet_project_item['cup'] = cup
                    try:
                        time_object = dt.strptime(time.strip(), "%H:%M")
                    except ValueError:   # IGNORE LIVE MATCHES
                        continue
                    match_datetime = datetime.datetime(dt.now().year, struct_time.month, struct_time.day, time_object.hour,
                                                       time_object.minute)
                    bet_project_item['time'] = match_datetime.isoformat()
                    bet_project_item['player1'] = players[players_cursor].strip()
                    players_cursor += 2     # between both players names is span with letter 'v'
                    bet_project_item['player2'] = players[players_cursor].strip()
                    players_cursor += 1
                    bet1 = bets[bets_cursor].strip()
                    if not bet1 or bet1 == '':
                        bet1 = '0.0'
                    bets_cursor += 1
                    bet_project_item['bet_1'] = bet1
                    bet2 = bets[bets_cursor].strip()
                    if not bet2 or bet2 == '':
                        bet2 = '0.0'
                    bets_cursor += 1
                    bet_project_item['bet_x'] = bet2
                    bet3 = bets[bets_cursor].strip()
                    if not bet3 or bet3 == '':
                        bet3 = '0.0'
                    bets_cursor += 1
                    bet_project_item['bet_2'] = bet3
                    logging.warning(bet_project_item)
                    yield bet_project_item
                except:
                    logging.exception('exception occured')
                    continue
