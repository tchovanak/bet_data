import scrapy
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import logging
import threading
import time
from bet_project.items import BetLiveItem
from pyvirtualdisplay import Display
import datetime 

def setInterval(interval):
	def decorator(function):
		def wrapper(*args, **kwargs):
		    stopped = threading.Event()

		    def loop(): # executed in another thread
		        while not stopped.wait(interval): # until stopped
		        	kwargs['stop'] = stopped
		            	function(*args, **kwargs)

		    t = threading.Thread(target=loop)
		    t.daemon = True # stop if the program exits
		    t.start()
		    return stopped
		return wrapper
	return decorator


class SuperBetLiveSpider(scrapy.Spider):

	name = "superbet_live"
	allowed_domains = ["superbet.ro"]
	start_urls = ['https://www.superbet.ro/pariuri-live']
	player1 = ""
	player2 = ""
	custom_settings = {
	    'ITEM_PIPELINES': {
	        'bet_project.pipelines.LiveMatchPipeline': 400,
	    }
	}
	items = []

	def __init__(self, player1, player2,path, **kwargs):
		# self.driver = webdriver.PhantomJS("g:/workspace_bet_scraper/phantomjs-2.1.1-windows/bin/phantomjs.exe")
		self.display = Display(visible=0, size=(1400, 1024))
		self.display.start()
		self.driver = webdriver.Firefox()
		self.player1 = player1
		self.player2 = player2
		self.path = path
		self.driver.set_window_size(1400, 1024)
		self.wait = WebDriverWait(self.driver, 20)

	def parse(self, response):
		self.driver.get(response.url)
		
		iframe = WebDriverWait(self.driver,30, 1).until(
			EC.presence_of_element_located(
				(By.XPATH, '//iframe[contains(@id,"seven-plugin-live")]')))
		self.driver.switch_to_frame(iframe)
		WebDriverWait(self.driver,1000, 10).until(
			EC.presence_of_element_located(
				(By.XPATH, '//span[contains(@class,"match-title-text") and contains(text(),"'+str(self.player1)+'") and contains(text(),"'+str(self.player2)+'")]')))

		stop = self.get_odds()

		self.time_to_end = datetime.datetime.now() + datetime.timedelta(minutes=130)
		
		while not stop.wait(60):
			if datetime.datetime.now() > self.time_to_end:
				stop.set()

		for item in self.items:
			yield item

	@setInterval(10.0)
	def get_odds(self,*args, **kwargs):
		try: 
			outer_div_element = WebDriverWait(self.driver,5).until(
				EC.presence_of_element_located(
				(By.XPATH, '//span[contains(@class,"match-title-text") and '
					'(contains(text(),"'+self.player1+'") and '
					'contains(text(),"'+self.player2+'"))]/ancestor::div[contains(@class,"table-row match")]')))
		except TimeoutException:
			logging.warning('SUPERBET_LIVE: Match ended ' + self.player1 + ' v ' + self.player2)
			return			
		try:
			selector = scrapy.Selector(text=outer_div_element.get_attribute('innerHTML'))
			time = selector.xpath('.//div[contains(@class,"match-time")]//text()').extract()
			score = selector.xpath('.//div[contains(@class,"match-result")]//text()').extract()
			bets = selector.xpath('.//div[contains(@class,"match-bets-items")]//span[contains(@class,"table-button-value")]//text()').extract()
			logging.warning(time)
		except:
			return
		if not bets or len(bets) < 3:
			bets = ["0.0","0.0","0.0"]
		if time[0].strip == 'AOT':
			stop = kwargs['stop']
			stop.set()
			return
		try:
			bet1 = bets[0].strip()
			betx = bets[1].strip()
			bet2 = bets[2].strip()
			betliveitem = BetLiveItem()
			betliveitem['player1'] = self.player1
			betliveitem['player2'] = self.player2
			betliveitem['bet_1'] = bet1
			betliveitem['bet_x'] = betx
			betliveitem['bet_2'] = bet2
			if len(score) > 1:
				betliveitem['score'] = score[1].strip()
			betliveitem['act_time'] = datetime.datetime.now()
			if time and len(time) > 0:
				betliveitem['time'] = time[0].strip()
			self.items.append(betliveitem)
		except:
			logging.exception("BETPUMP LIVE: Exception ")
			return
