# -*- coding: utf-8 -*-
import scrapy
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import logging
from bet_project.items import BetProjectItem
from datetime import datetime as dt
import datetime
import re
from selenium.common.exceptions import TimeoutException
from pyvirtualdisplay import Display


class SuperBetPrematchSpider(scrapy.Spider):
    """
        Crawler for superbet.ro
    """
    name = "superbet_prematch"
    allowed_domains = ["superbet.ro"]
    wait_long = 20  # how many seconds should webdriver wait for response when it is not avoidable  
    wait_short = 5 # how many seconds should webdriver wait for response that is avoidable
    start_urls = ['https://www.superbet.ro/pariuri-sportive/toate']
    custom_settings = {
        'ITEM_PIPELINES': {
            'bet_project.pipelines.BetProjectPipeline': 300,
        }
    }

    # for roman language translation of weekdays
    weekdays_dict = {"lun": 0, "mar": 1, "mie": 2, "joi": 3, "vin": 4, "sâm": 5, "dum": 6}

    def __init__(self, **kwargs):
        #self.display = Display(visible=0, size=(1400, 1024))
        #self.display.start()
        #self.driver = webdriver.Firefox()
        self.driver = webdriver.PhantomJS()
        self.driver.set_window_size(1400, 1024)
        self.wait = WebDriverWait(self.driver, self.wait_long)

    def parse(self, response):
        self.driver.get(response.url)
        try:
            link = self.wait.until(EC.presence_of_element_located((By.XPATH,
                                   '//li/a/label[@class="checkbox" and text()=" Fotbal"]')))
            self.driver.execute_script("arguments[0].click();", link)
        except TimeoutException:
            logging.exception('SUPERBET_PREMATCH: Timeout exception when trying to access xpath : '
                              '//li/a/label[@class="checkbox" and text()=" Fotbal"]')
            return
        try:
            link2 = self.wait.until(EC.presence_of_element_located((By.XPATH,
                                    '//li/a/label[@class="checkbox" and text()=" Fotbal"]/input')))
            self.driver.execute_script("arguments[0].click();", link2)
        except TimeoutException:
            logging.exception('SUPERBET: Timeout exception when trying to access xpath : '
                              '//li/a/label[@class="checkbox" and text()=" Fotbal"]/input')
            return

        selector = scrapy.Selector(text=self.driver.page_source)
        try:
            links = selector.xpath('//h1[contains(text(),"FOTBAL:")]')
            for link in links:   # there may be multiple blocks for same league for example Mexico Cupa MX
                try:
                    table = link.xpath('./ancestor::div[@class="tour"]//table[@class="offer"]')
                    players = table.xpath('.//tr/td[@class="match"]//text()').extract()
                    times = table.xpath('.//tr/td[@class="date"]//text()').extract()
                    odds = table.xpath('.//tr/td[@class="odd" or @class="noodd"]//text()').extract()
                except:
                    logging.exception("Exception")
                    continue
                player_counter = 0
                odd_counter = 0
                for t in times:
                    try:
                        bet_project_item = BetProjectItem()
                        bet_project_item['cup'] = link.xpath('.//text()').extract()[0][7:]
                        try:
                            date1 = dt.strptime(t.encode('utf-8').strip(),'%d.%m. %H:%M')
                            if dt.now() > date1:
                                time_object = datetime.datetime(dt.now().year, date1.month, date1.day, date1.hour, date1.minute)
                            else: 
                                time_object = datetime.datetime(dt.now().year + 1, date1.month, date1.day, date1.hour, date1.minute)
                        except ValueError:
                            splitted = t.strip().split(" ")     # time is in format: joi 20:00
                            weekday = self.weekdays_dict[splitted[0].encode('utf-8')]  # weekday is in roman language i use dictionary to translate
                            now_weekday = dt.now().weekday()
                            diff1 = weekday - now_weekday
                            if diff1 < 0:
                                diff1 = (7 - (now_weekday+1)) + (weekday+1)
                            date1 = dt.now() + datetime.timedelta(days=diff1)
                            strdate = str(date1.day) + " " + splitted[1]
                            date2 = dt.strptime(strdate, "%d %H:%M")
                            time_object = datetime.datetime(date1.year, date1.month, date1.day, date2.hour,
                                                                         date2.minute)
                        time_object = time_object - datetime.timedelta(hours=1)     # time correction because its +2:00 UTC time
                        bet_project_item['time'] = time_object.isoformat()
                        
                        player_counter += 1
                        splitted = re.split("\xb7", players[player_counter].strip())
                        bet_project_item['player1'] = splitted[0]
                        bet_project_item['player2'] = splitted[1]
                        player_counter += 1
                        if len(odds)  > odd_counter:
                            if odds[odd_counter].strip() == '-':
                                bet_project_item['bet_1'] = '0.0'
                            else:
                                bet_project_item['bet_1'] = odds[odd_counter]
                        else:
                            bet_project_item['bet_1'] = '0.0'
                        odd_counter += 1
                        if len(odds)  > odd_counter:
                            if odds[odd_counter].strip() == '-':
                                bet_project_item['bet_x'] = '0.0'
                            else:
                                bet_project_item['bet_x'] = odds[odd_counter]
                        else:
                            bet_project_item['bet_x'] = '0.0'
                        odd_counter += 1
                        if len(odds)  > odd_counter:
                            if odds[odd_counter].strip() == '-':
                                bet_project_item['bet_2'] = '0.0'
                            else:
                                bet_project_item['bet_2'] = odds[odd_counter]
                        else:
                            bet_project_item['bet_2'] = '0.0'
                        odd_counter += 4
                        yield bet_project_item
                    except:
                        logging.exception('exception occured')
                        continue

        except:
            logging.exception('SUPERBET_PREMATCH: ')
            return

        self.driver.close()

