# -*- coding: utf-8 -*-
import scrapy
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime as dt
from bet_project.items import BetProjectItem
import datetime
import logging
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from pyvirtualdisplay import Display


class VBetSpider(scrapy.Spider):
    """
        Crawler for mobile.bet365.com
    """
    name = "vbet_prematch"
    allowed_domains = ["vbet.com"]
    wait_long = 30  # how many seconds should webdriver wait for response when it is not avoidable  
    wait_short = 15 # how many seconds should webdriver wait for response that is avoidable
    start_urls = ['https://www.vbet.com/?lang=eng']
    custom_settings = {
        'ITEM_PIPELINES': {
            'bet_project.pipelines.BetProjectPipeline': 300,
        }
    }

    def __init__(self, **kwargs):
        self.display = Display(visible=0, size=(1400, 1024))
        self.display.start()
        #self.driver = webdriver.PhantomJS()
        self.driver = webdriver.Firefox()
        self.driver.set_window_size(1400, 1024)
        self.wait = WebDriverWait(self.driver, self.wait_long)

    def parse(self, response):
        self.driver.get("https://www.vbet.com/#/?lang=eng")
        attempts = 0
        result = False
        while attempts <= 5:   # this section throws stale element exception sometimes therefore
                               # it is tried multiple times
            try:
                link = self.wait.until(EC.presence_of_element_located((By.XPATH,
                                       '//span[text()="Sports"]')))
                self.driver.execute_script("arguments[0].click();", link)
                
                link2 = WebDriverWait(self.driver, self.wait_short).until(EC.presence_of_element_located((By.XPATH,
                                        '//li[contains(@id, "sport-favoritecompetitions")]/div[contains(@class, "active")]'))) # 
                self.driver.execute_script("arguments[0].click();", link2)                    
                link2 = self.wait.until(EC.presence_of_element_located((By.XPATH,
                                        '//li[contains(@id,"sport-Soccer")]/div[not(contains(@class, "active"))]')))
                self.driver.execute_script("arguments[0].click();", link2)
                result = True
                break
            except StaleElementReferenceException:
                if attempts == 5:
                    logging.exception("Exception")
                attempts += 1
            except TimeoutException:
                attempts += 1
                logging.warning('VBET: Timeout exception when trying to access xpath.')
        if not result:
            return

        result = False
        while attempts <= 5: 
            try:
                links = WebDriverWait(self.driver, self.wait_short).until(EC.presence_of_all_elements_located((By.XPATH,
                                      '//li[contains(@id,"sport-Soccer")]//div[contains(@class, "region-item-v3") and not(contains(@class, "active"))]/p')))
                for link in links:
                    self.driver.execute_script("arguments[0].click();", link)
                result = True
                break
            except StaleElementReferenceException :
                if attempts == 5:
                    logging.exception("Too many attempts with stale element reference exception")
                    break
                attempts += 1
            except TimeoutException:
                logging.exception('VBET: Timeout exception when trying to access xpath.')
                attempts += 1
                continue
        if not result:
            return

        result = False
        while attempts <= 5: 
            try:
                links = WebDriverWait(self.driver, self.wait_long).until(EC.presence_of_all_elements_located((By.XPATH,
                                  '//li[contains(@id,"sport-Soccer")]//div[contains(@class, "competition-title-v3")]//label')))
                result = True
                break
            except StaleElementReferenceException :
                if attempts == 5:
                    logging.exception("Too many attempts with stale element reference exception")
                    break
                attempts += 1
            except TimeoutException:
                logging.exception('VBET: Timeout exception when trying to access xpath.')
                attempts += 1
                continue

        if not result:
            return

        size = len(links)
        for i in range(0, size):
            attempts = 0
            result = False
            if i >= len(links):
                return
            while attempts <= 5: 
                try:
                    links = WebDriverWait(self.driver, self.wait_long).until(EC.presence_of_all_elements_located((By.XPATH,
                                      '//li[contains(@id,"sport-Soccer")]//div[contains(@class, "competition-title-v3")]//label')))
                    result = True
                    break
                except StaleElementReferenceException :
                    if attempts == 5:
                        logging.exception("Too many attempts with stale element reference exception")
                        break
                    attempts += 1
                except TimeoutException:
                    logging.exception('VBET: Timeout exception when trying to access xpath.')
                    attempts += 1
                    continue
            if not result:
                continue
            link = links[i]
            title = link.get_attribute("title")
            self.driver.execute_script("arguments[0].click();", link)
            try:
                region = self.wait.until(EC.presence_of_all_elements_located((By.XPATH,
                                    './/div[contains(@class,"time-title-view") or contains(@class,"time-game") or contains(@class,"mini-box-market-info")]'))) #
                region = self.wait.until(EC.presence_of_element_located((By.XPATH,
                                    '//h2[contains(span[3]/text(),"'+title+'")]/b'))) #
                region = region.get_attribute('title')
            except:
                logging.exception('VBET: Timeout exception when trying to access xpath.')
                continue
            try:
                selector = scrapy.Selector(text=self.driver.page_source)
                games_list = selector.xpath('//div[contains(@class,"prematch-games-list")]')
                if not games_list or len(games_list) == 0:
                    continue
                divstolook = games_list[0].xpath('.//div[contains(@class,"time-title-view") or contains(@class,"time-game") or '
                                                 'contains(@class,"mini-box-market-info")]')
                league = selector.xpath('//div[contains(@class,"prematch-column-title-v3")]/h2/span[3]//text()').extract()
                if not league or len(league) == 0:
                    continue
                else:
                    league = league[0].strip()
                league = region + ' ' + league
                actual_date = None
                bet_project_item = BetProjectItem()
                bet_project_item['cup'] = league
                for div in divstolook:
                    try:
                        cl = div.xpath('./@class').extract()[0]
                        if "time-title-view" in cl:
                            dat = div.xpath('.//p//text()').extract()
                            if not dat or len(dat) == 0:
                                continue
                            else:
                                dat = dat[0].strip()
                            actual_date = dt.strptime(dat, '%d %b %Y')
                        elif "time-game" in cl:
                            gametime = div.xpath('.//p//text()').extract()
                            if not gametime or len(gametime) == 0:
                                continue
                            else:
                                gametime = gametime[0].strip().split(':')
                            if actual_date:
                                actual_time = datetime.datetime(actual_date.year, actual_date.month, actual_date.day,
                                                            int(gametime[0]), int(gametime[1]))
                                bet_project_item['time'] = actual_time.isoformat()
                        elif "mini-box-market-info" in cl:
                            player = div.xpath('.//i/text()').extract()
                            if not player or len(player) == 0:
                                player = 'Unknown'
                            else:
                                player = player[0].strip()
                            odd = div.xpath('./span//text()').extract()
                            if not odd or len(odd) == 0:
                                odd = '0.0'
                            else:
                                odd = odd[0].strip()
                            if 'X' in player:
                                bet_project_item['bet_x'] = odd
                            elif "player1" in bet_project_item.keys() and bet_project_item['player1'] is not None:
                                bet_project_item['player2'] = player
                                bet_project_item['bet_2'] = odd
                                logging.warning(bet_project_item)
                                yield bet_project_item
                                bet_project_item = BetProjectItem()
                                bet_project_item['cup'] = league
                            else:
                                bet_project_item['player1'] = player
                                bet_project_item['bet_1'] = odd
                    except:
                        logging.exception('Exception')
                        continue
            except:
                logging.exception("Exception")
                continue


        self.driver.close()



