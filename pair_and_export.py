# -*- coding: utf-8 -*-
import psycopg2
from datetime import datetime as dt
from difflib import SequenceMatcher
import csv
import datetime
import sys

dict_teams = {}
dict_leagues = {}
list_ignores = []

print "PAIRING"

print sys.argv

with open('teams.csv') as csvfile:
     reader = csv.reader(csvfile,delimiter = ',')
     for row in reader:
     	dict_teams.update({row[0]:row[1]})

with open('leagues.csv') as csvfile:
     reader = csv.reader(csvfile,delimiter = ',')
     for row in reader:
     	dict_leagues.update({row[0].strip():row[1]})

with open('ignores.csv') as csvfile:
     reader = csv.reader(csvfile,delimiter = ',')
     for row in reader:
     	list_ignores.append(row[0])


from_day = dt.strptime(str(sys.argv[1]), '%Y-%m-%dT%H:%M:%S') 
to_day =  dt.strptime(str(sys.argv[2]), '%Y-%m-%dT%H:%M:%S')


spiders = ["gowager_prematch","betpump_prematch","bet365_prematch", "vbet_prematch", "superbet_prematch"]

connections = []

for spider in spiders:
	conn = psycopg2.connect(database = spider, user='postgres', host='localhost', port=5432, password='password')
	connections.append({'spider':spider, 'conn':conn})

all_data = []

for connection in connections:
	conn = connection['conn']
	cur = conn.cursor()
	cur.execute("""SELECT * FROM items WHERE act_time BETWEEN timestamp %s AND timestamp %s;""", (from_day,to_day))
	all_fetched = cur.fetchall()
	spider = connection['spider']

	for f in all_fetched:
		item = {'act_time':f[0], 'time':f[1],'player1':f[2],'player2':f[3],'competition':f[4],'odd_1':f[5], 'odd_x':f[6], 'odd_2':f[7], 'from':spider} 
		all_data.append(item)

all_data = sorted(all_data, key=lambda x: (x['time']))   # sort 

def replacing(item):
	it = item.decode('utf-8').upper().encode('utf-8')
	for r in list_ignores:
		it = it.replace(r.strip() + ' ','')
		it = it.replace(' ' + r.strip(),'')
	return it.translate(None, '- .')
	
def similarity(a,b):
    return SequenceMatcher(None, a, b).ratio()

# FOR DEBUGGING PURPOSE
def show_result(item, item2, player1_1, player1_2, player2_1, player2_2, sim_player1, sim_player2, sim_league):
	print "-----------------------------"
	print item['player1']
	print player1_1
	print item2['player1']
	print player2_1
	print item['player2']
	print player1_2
	print item2['player2']
	print player2_2
	print item['time']
	print item2['time']
	print item['competition']
	print item2['competition']
	print sim_player1
	print sim_player2
	print sim_league
	print "-----------------------------"

export_data = []



first_record = 0
last_date = None
i = -1
dont_visit = []
while i < len(all_data) - 1:
	competition_id = None
	i += 1
	item = all_data[i]
	if i in dont_visit:
		continue
	first_record = i
	if last_date is None or item['time'] != last_date:
		last_date = item['time']
	#	first_record = i
	if item['competition'].strip() in dict_leagues:
		competition_id = dict_leagues[item['competition'].strip()]
	first_item = {'competition_id':competition_id, 'competition':item['competition'].strip(),'time':item['time'], 'act_time':item['act_time'], 'competition_name_' + item['from']:item['competition'],
				  'player1':item['player1'], 'player2':item['player2'], item['from'] + '_odd_1':item['odd_1'], item['from'] + '_odd_x':item['odd_x'], item['from'] + '_odd_2':item['odd_2'], 'not_paired': 'N'} 
	paired_items = {item['act_time']: first_item}
	
	for j in range(first_record + 1, len(all_data)):
		
		if j in dont_visit:
			continue
		
		item2 = all_data[j]

		diff = 0.0
		if item2['time'] != last_date:
			time1 = dt.strptime(item['time'], '%Y-%m-%dT%H:%M:%S') 
			time2 = dt.strptime(item2['time'], '%Y-%m-%dT%H:%M:%S') 
			diff = abs(time1 - time2)			
			if diff > datetime.timedelta(hours = float(sys.argv[4])):
				break	

		competition_id = None
		if item['competition'].strip() in dict_leagues:
			competition_id = dict_leagues[item['competition'].strip()]
		elif item2['competition'].strip() in dict_leagues:
			competition_id = dict_leagues[item2['competition'].strip()]
		
		sim_league = 0.0	
		if item['competition'].strip() in dict_leagues and item2['competition'].strip() in dict_leagues:
			if dict_leagues[item['competition'].strip()] == dict_leagues[item2['competition'].strip()]:
				sim_league = 1.0 
			else:
				#print 'IN DICTIONARY BUT DIFFERENT - COMPETITION'
				#show_result(item, item2, player1_1, player1_2, player2_1, player2_2, sim_player1, sim_player2, sim_league)
				continue
		else:
			sim_league = similarity(item['competition'].strip().upper().translate(None,"-  áéíóúščťžýľď"),item2['competition'].strip().upper().translate(None,"-  áéíóúščťžýľď"))
		
		player1_1 = replacing(item['player1'])
		player2_1 = replacing(item2['player1'])
		player1_2 = replacing(item['player2'])
		player2_2 = replacing(item2['player2'])
		
		sim_player1 = 0.0
		if item['player1'] in dict_teams and item2['player1'] in dict_teams:
			if dict_teams[item['player1']] == dict_teams[item2['player1']]:
				sim_player1 = 1.0
				#show_result(item, item2, player1_1, player1_2, player2_1, player2_2, sim_player1, sim_player2, sim_league)
			else:
				continue
		else:
			sim_player1 = similarity(player1_1, player2_1)
			
		sim_player2 = 0.0
		if item['player2'] in dict_teams and item2['player2'] in dict_teams:
			if dict_teams[item['player2']] == dict_teams[item2['player2']]:
				sim_player2 = 1.0
				#show_result(item, item2, player1_1, player1_2, player2_1, player2_2, sim_player1, sim_player2, sim_league)
			else:
				continue
		else:
			sim_player2 = similarity(player1_2, player2_2)
		
		
		#if diff > datetime.timedelta(hours = float(sys.argv[4])):
			#if (sim_player1 + sim_player2 + sim_league) < (float(sys.argv[3]) + 0.2):
				#continue
				#break
			#print "WRONG TIME BUT SIMILAR"
				#show_result(item, item2, player1_1, player1_2, player2_1, player2_2, sim_player1, sim_player2, sim_league)

		to_continue = True

		if item['from'] == item2['from'] and item2['act_time'] == item['act_time']:
			continue
		
		if sim_player1 + sim_player2 + sim_league > float(sys.argv[3]):
			to_continue = False

		if to_continue:	
			continue

		if ('Scunthorpe' in item['player1'] or 'Scunthorpe' in item2['player1']):
				show_result(item, item2, player1_1, player1_2, player2_1, player2_2, sim_player1, sim_player2, sim_league)

		
		#	print item['competition']
		#	print item
		#	print item2
		#	print i
		
		dont_visit.append(j)

		if item2['act_time'] in paired_items:
			p = paired_items[item2['act_time']]
			#if item2['from'] + "_odd_1" in p:
			#	continue 
			p[item2['from'] + "_odd_1"] = item2['odd_1']
			p[item2['from'] + "_odd_x"] = item2['odd_x']
			p[item2['from'] + "_odd_2"] = item2['odd_2']
			p['not_paired'] = 'P'
			p['competition_name_' + item2['from']] = item2['competition']
			p['competition_id'] = competition_id
			paired_items[item2['act_time']] = p
		else:
			first_item = {'competition_id': competition_id, 'competition':item2['competition'].strip(), 'time':item2['time'], 'act_time':item2['act_time'], 'competition_name_' + item2['from']:item2['competition'],
				  'player1':item['player1'], 'player2':item['player2'], item2['from'] + '_odd_1':item2['odd_1'], item2['from'] + '_odd_x':item2['odd_x'],
				   item2['from'] + '_odd_2':item2['odd_2'], 'not_paired':'N'} 
			paired_items[item2['act_time']] = first_item

	for p in paired_items:
		export_data.append(paired_items[p])


with open(str(sys.argv[5]) + 'prematch_data_'+str(datetime.datetime.now().isoformat())+'.csv', 'wb') as csvfile:
	dictwriter = csv.DictWriter(csvfile, ['competition_id', "not_paired",'competition',
										"competition_name_bet365_prematch",
										"competition_name_superbet_prematch", 
										"competition_name_gowager_prematch", 
										"competition_name_betpump_prematch",
										"competition_name_vbet_prematch",
										"player1","player2","time", "act_time",
										"bet365_prematch_odd_1","bet365_prematch_odd_x","bet365_prematch_odd_2",
										"superbet_prematch_odd_1","superbet_prematch_odd_x","superbet_prematch_odd_2",
										"vbet_prematch_odd_1","vbet_prematch_odd_x","vbet_prematch_odd_2",
										"gowager_prematch_odd_1","gowager_prematch_odd_x","gowager_prematch_odd_2",
										"betpump_prematch_odd_1","betpump_prematch_odd_x","betpump_prematch_odd_2"])
	dictwriter.writeheader()
	for item in export_data:
		if 'competition_name_vbet_prematch' in item and item['competition_name_vbet_prematch'] != '':
			item['competition'] = item['competition_name_vbet_prematch'].strip()
		elif 'competition_name_betpump_prematch' in item and item['competition_name_betpump_prematch'] != '':
			item['competition'] = item['competition_name_betpump_prematch'].strip()
		elif 'competition_name_bet365_prematch' in item and item['competition_name_bet365_prematch'] != '':
			item['competition'] = item['competition_name_bet365_prematch'].strip()
		elif 'competition_name_superbet_prematch' in item and item['competition_name_superbet_prematch'] != '':
			item['competition'] = item['competition_name_superbet_prematch'].strip()
		elif 'competition_name_gowager_prematch' in item and item['competition_name_gowager_prematch'] != '':
			item['competition'] = item['competition_name_gowager_prematch'].strip()
		
		
		
		dictwriter.writerow(item)

# export dictionary to csv that can be imported to google spreadsheet

