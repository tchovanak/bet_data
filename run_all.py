#!/usr/bin/python

from twisted.internet import reactor, defer
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
import bet_project.spiders.bet365_spider
import bet_project.spiders.gowager_spider
import bet_project.spiders.betpump_spider
import bet_project.spiders.vbet_spider
import bet_project.spiders.superbet_spider
from scrapy.utils.project import get_project_settings
from scrapy import settings
import datetime


configure_logging(get_project_settings())
settings = get_project_settings()
settings.set('DOWNLOAD_TIME',datetime.datetime.now())
runner = CrawlerRunner(settings)

# Runs all spiders sequentially


@defer.inlineCallbacks
def crawl():
    yield runner.crawl(bet_project.spiders.gowager_spider.GoWagerSpider)
    yield runner.crawl(bet_project.spiders.bet365_spider.Bet365Spider)
    yield runner.crawl(bet_project.spiders.betpump_spider.BetPumpSpider)
    yield runner.crawl(bet_project.spiders.vbet_spider.VBetSpider)
    yield runner.crawl(bet_project.spiders.superbet_spider.SuperBetSpider)
    reactor.stop()

crawl()
reactor.run()           # the script will block here until the last crawl call is finished
