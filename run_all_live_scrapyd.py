from twisted.internet import reactor, defer
from scrapy.crawler import CrawlerRunner, CrawlerProcess
from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings
from scrapyd_api import ScrapydAPI
import datetime
import requests
import sys

#url = 'http://localhost:6800/schedule.json'
#r = requests.post(url, data={"project": "bet_project";"spider": "gowager"})
#response_json = r.json()
#print response_json
# Runs all spiders with scrapyd service

spiders_list = ["gowager_live", "superbet_live","betpump_live", "betfair_live" ]

scrapyd = ScrapydAPI('http://localhost:6800')
#settings = {'DOWNLOAD_TIME': datetime.datetime.now().isoformat()}

#print scrapyd.cancel('bet_project', 'e5552d0658e211e69d526036dd68a5ce')
#print scrapyd.cancel('bet_project', 'e5552d0658e211e69d526036dd68a5ce')
#print scrapyd.cancel('bet_project', 'e556d94458e211e69d526036dd68a5ce', signal="TERM")


for spider in spiders_list:
	print scrapyd.schedule("bet_project", spider, player1 = str(sys.argv[1]), player2 = str(sys.argv[2]), path=str(sys.argv[3]))

    
