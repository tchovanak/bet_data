from twisted.internet import reactor, defer
from scrapy.crawler import CrawlerRunner, CrawlerProcess
from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings
from scrapyd_api import ScrapydAPI
import datetime
import requests
import sys

#url = 'http://localhost:6800/schedule.json'
#r = requests.post(url, data={"project": "bet_project";"spider": "gowager"})
#response_json = r.json()
#print response_json
# Runs all spiders with scrapyd service

if len(sys.argv) > 1:
	spiders_list = sys.argv[1:]
else:
	spiders_list = ["gowager_prematch", "betpump_prematch", "bet365_prematch", "vbet_prematch", "superbet_prematch"]

scrapyd = ScrapydAPI('http://localhost:6800')
settings = {'DOWNLOAD_TIME': datetime.datetime.now().isoformat()}
for spider in spiders_list:
    print scrapyd.schedule('bet_project', spider, settings=settings)