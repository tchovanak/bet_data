#!/bin/sh

# installs postgresql and pgadmin https://help.ubuntu.com/community/PostgreSQL
sudo apt-get install postgresql postgresql-contrib
sudo apt-get install pgadmin3

# install python git and pip # python should be preinstalled 
sudo apt-get install git
sudo apt-get -y install python-pip

# installs scrapy and scrapyd server 
sudo pip install scrapy
sudo pip install selenium
sudo pip install gspread
sudo pip install --upgrade oauth2client
sudo pip install scrapyd
sudo pip install scrapyd-client
sudo pip install python-scrapyd-api

# to run browser in background 
sudo apt-get install xvfb
sudo pip install pyvirtualdisplay
