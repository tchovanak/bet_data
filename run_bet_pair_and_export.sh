#!/bin/sh

cd /home/tomas/workspace_bet_project
 
if /etc/init.d/postgresql status | grep "online" > /dev/null
then
echo "postgres service running, everything is fine"
else
echo "postgres is not running"
/etc/init.d/postgresql start 
fi

python pair_and_export.py $1 $2 $3 $4

