#!/bin/sh

cd /home/tomas/workspace_bet_project

if /etc/init.d/postgresql status | grep "online" > /dev/null
then
echo "postgres service running, everything is fine"
else
echo "postgres is not running"
/etc/init.d/postgresql start 
fi

scrapyd & >/dev/null
scrapyd-deploy >/dev/null  
python run_all_with_scrapyd_service.py
 
